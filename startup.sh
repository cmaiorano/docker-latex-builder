#! /bin/bash

find . -name "*.bbl" -type f -delete
#find . -name "*.out" -type f -delete
#find . -name "*.log" -type f -delete
#find . -name "*.aux" -type f -delete
#find . -name "*.lof" -type f -delete
#find . -name "*.bcf" -type f -delete
#find . -name "*.toc" -type f -delete
#find . -name "*.blg" -type f -delete

pdflatex -synctex=1 -interaction=nonstopmode $MAIN_FILE.tex